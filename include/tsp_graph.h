/**
 * \file
 * \brief 	Graph of a travelling salesman problem (TSP)
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef TSP_GRAPH_H_INCLUDED
#define TSP_GRAPH_H_INCLUDED

/**
 * \struct Node
 * \brief A node of the TSP graph, corresponds to a town
 */
typedef struct
{
	float x;   /**< The x-coordinate of the town */
	float y;   /**< The y-coordinate of the town */
} Node;

/**
 * \struct TSPGraph
 * \brief A TSP graph, holding a set of towns
 */
typedef struct
{
	Node* towns;   /**< The towns */
	int size;      /**< The number of towns */
} TSPGraph;

/**
 * \brief Loads a TSP graph from a file
 * \param[out] graph The TSP graph
 * \param[in] file_path The path to the file
 * \param[in] size The number of towns to load from the file
 * \return 0 on success, a negative value on failure (invalid file or out-of-memory)
 *
 * Each line of the file must correspond to a town, and containing the x- and y-coordinate of the
 * towns listed in that order and separated by a space.
 * Exemple of valid three-town file content :
 * 200.0 300.0
 * 100.0 150.5
 * 300.0 50.0
 *
 * If the file contains more towns than the specified size n, only first n towns will be loaded.
 */
int tspgraph_load(TSPGraph* graph, char* file_path, int size);

/**
 * \brief Computes the Euclidean distance between two towns in TSP graph
 * \param[in] graph The TSP graph
 * \param[in] i The first town index
 * \param[in] j The second town index
 * \return The Euclidean distance between the two towns
 */
float tspgraph_compute_dist(TSPGraph const* graph, int i, int j);

/**
 * \brief Gets the size of a TSP graph
 * \param[in] graph The TSP graph
 * \return The number of towns contained in the graph
 */
int tspgraph_get_size(TSPGraph const* graph);

/**
 * \brief Creates a subgraph of a TSP graph
 * \param[in] graph The TSP graph
 * \param[in] subsize The number of towns that the subgraph must contain (at most the size of the 
 *                    original graph)
 * \param[out] subgraph The resulting subgraph containing the first subsize towns of the original graph
 *
 * Note that the subgraph set of towns is shallow copy of the original graph set of towns, the 
 * subgraph must therefore not be freed and the original graph must not be freed until the subgraph is 
 * no more in use.
 */  
void tspgraph_get_subgraph(TSPGraph const* graph, int subsize, TSPGraph* subgraph);

/**
 * \brief Prints a TSP graph to the standard output
 * \param[in] graph The TSP graph
 *
 * The graph is printed as a list of towns location.
 */
void tspgraph_print(TSPGraph const* graph);

/**
 * \brief Frees all the memory associated with TSP graph
 * \param[in] graph The TSP graph
 */
void tspgraph_free(TSPGraph* graph);

#endif // TSP_GRAPH_H_INCLUDED

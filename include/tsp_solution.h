/**
 * \file
 * \brief 	Represents a solution of a travelling salesman problem (TSP)
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef TSP_SOLUTION_H_INCLUDED
#define TSP_SOLUTION_H_INCLUDED

/**
 * \struct TSPSolution
 * \brief A solution of a TSP
 */
typedef struct
{
	int* route;      /**< The list of the TSP towns, in the order they are visited */
	float eval;      /**< The evaluation of the solution (total distance traveled) */
	int town_count;  /**< The number of towns visited */
} TSPSolution;

/**
 * \brief Initializes a TSP solution
 * \param[out] sol The initialized TSP solution
 * \param[in] town_count The number of town that the solution must contain
 * \return 0 on success, a negative value on failure (out-of-memory)
 */
int tspsol_init(TSPSolution* sol, int town_count);

/**
 * \brief Prints a TSP solution to the standard output
 * \param[in] sol The TSP solution
 */
void tspsol_print(TSPSolution const* sol);

/**
 * \brief Frees all the memory associated with a TSP solution
 * \param[in] sol The TSP solution
 *
 * Note that calling this function on a resulting solution of <tspsol_init> is always safe, even in
 * the event the initialization would have failed (in that case this function has no effect).
 */
void tspsol_free(TSPSolution* sol);

#endif // TSP_SOLUTION_H_INCLUDED

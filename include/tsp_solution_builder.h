/**
 * \file
 * \brief 	Travelling salesman problem (TSP) solution builder
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef TSP_SOLUTION_BUILDER
#define TSP_SOLUTION_BUILDER

#include <dist_matrix.h>
#include <tsp_solution.h>

/**
 * \struct TSPSolutionBuilder
 * \brief Builder of a TSP solution
 */
typedef struct
{
	Edge* chosen_edges;    /**< The set of edges contained in the solution route */
	int* src_index_map;    /**< Maps source towns with their corresponding edge if any */
	int* dest_index_map;   /**< Maps destination towns with their corresponding edge if any */
	int* buffer;           /**< Buffer used to avoid repetitive malloc calls */
	int count;             /**< Number of edges currently in the solution */
} TSPSolutionBuilder;

/**
 * \brief Initializes a TSP solution builder
 * \param[out] builder The intialized TSP solution builder
 * \param[in] town_count The maximum number of towns the solution may contain
 * \return 0 on success, a negative number on failure (out-of-memory)
 */
int tspsolbuilder_init(TSPSolutionBuilder* builder, int town_count);

/**
 * \brief Adds a edge to the solution
 * \param[in] builder The TSP solution builder
 * \param[in] edge The edge to add
 *
 * The TSP solution builder must not be full, ie. must contains stricly less edges than the maximum 
 * number of towns given to <tspsolbuilder_init> when the builder was initialized.
 */
void tspsolbuilder_push_edge(TSPSolutionBuilder* builder, Edge const* edge);

/**
 * \brief Removes from the solution the last edge that was added to it with <tspsolbuilder_push_edge>
 * \param[in] builder The TSP solution builder
 *
 * The TSP solution builder must contains at least one edge.
 */
void tspsolbuilder_pop_edge(TSPSolutionBuilder* builder);

/**
 * \brief Gets the last edge that was added to the solution with <tspsolbuilder_push_edge>
 * \param[in] builder The TSP solution builder
 * \return The last edge that was added to the solution
 *
 * The TSP solution builder must contains at least one edge.
 */
Edge const* tspsolbuilder_peek_edge(TSPSolutionBuilder const* builder);

/**
 * \brief Gets the town directly preceding the specified town in the route of a TSP solution, if any
 * \param[in] builder The TSP solution builder
 * \param[in] dest_town The town for which to find the source town
 * \return The town directly preceding the specified town in the route of the TSP solution contained
 *         in the builder if there is one (>= 0), a stricly negative number otherwise.
 *
 * The source town is the town for which there exist in the solution a edge from this town to the
 * specified dest_town.
 * This function assumes there is at most one such edge in the builder.
 */
int tspsolbuilder_get_src_of(TSPSolutionBuilder const* builder, int dest_town);

/**
 * \brief Gets the town directly following the specified town in the route of a TSP solution, if any
 * \param[in] builder The TSP solution builder
 * \param[in] src_town The town for which to find the destination town
 * \return The town directly following the specified town in the route of the TSP solution contained
 *         in the builder if there is one (>= 0), a stricly negative number otherwise.
 *
 * The destination town is the town for which there exist in the solution a edge to this town from the
 * specified src_town.
 * This function assumes there is at most one such edge in the builder.
 */
int tspsolbuilder_get_dest_of(TSPSolutionBuilder const* builder, int src_town);

/**
 * \brief Builds the solution contained in a TSP solution builder
 * \param[in] builder The TSP solution builder
 * \param[in] sol_eval The evaluation of the solution contained in the builder
 * \param[out] sol The builded solution
 * \return 0 on success, a negative value otherwise (the solution is not valid)
 *
 * A solution is considered valid if and only if it represents a Hamiltonian cycle in the
 * graph of towns (ie. does not contains any subtour).
 */
int tspsolbuilder_build(TSPSolutionBuilder const* builder, float sol_eval, TSPSolution* sol);

/**
 * \brief Frees all the memory associated with a TSP solution builder.
 * \param[in] builder The TSP solution builder
 */
void tspsolbuilder_free(TSPSolutionBuilder* builder);

#endif // TSP_SOLUTION_BUILDER

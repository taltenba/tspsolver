/**
 * \file
 * \brief 	Evaluates the travelling salesman problem (TSP) solver performances given user input.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef TIME_CONTROLLER_H_INCLUDED
#define TIME_CONTROLLER_H_INCLUDED

/**
 * \brief Times the TSP solver for a range of graph size according to the given command-line arguments
 * \param[in] argc Number of command-line arguments
 * \param[in] argv The command-line arguments
 * \return 0 on success, a negative value on failure (invalid arguments or out-of-memory)
 */
int timecontroller_execute(int argc, char** argv);

#endif // TIME_CONTROLLER_H_INCLUDED

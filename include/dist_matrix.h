/**
 * \file
 * \brief 	Distance matrix of a travelling salesman problem (TSP)
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef DIST_MATRIX_H_INCLUDED
#define DIST_MATRIX_H_INCLUDED

#include <tsp_graph.h>

/**
 * \struct DistMatrix
 * \brief Distance matrix of a TSP
 */
typedef struct
{
	int* row_towns;   /**< Towns corresponding to each rows of the matrix (source towns) */
	int* col_towns;   /**< Towns corresponding to each columns of the matrix (destination towns) */
	float* dists;     /**< Coefficients of the distance matrix */
	int size;         /**< Order of the matrix (square matrix) */
} DistMatrix;

/**
 * \struct MatPos
 * \brief A position in a matrix
 */
typedef struct
{
	int x;   /**< The line */
	int y;   /**< The column */
} MatPos;

/**
 * \struct Edge
 * \brief An edge between two towns
 */
typedef struct
{
	int src;   /**< The source town */
	int dest;  /**< The destination town */
} Edge;

/**
 * \brief Initializes a distance matrix
 * \param[out] mat The distance matrix
 * \param[in] size The order of the matrix (square matrix)
 * \return 0 on success, a negative value on failure (out of memory)
 */
int distmat_init(DistMatrix* mat, int size);

/**
 * \brief Computes the distance matrix from a graph
 * \param[out] mat The distance matrix
 * \param[in] graph The graph containing exactly n town, where n is the order of the matrix
 *
 * Note that the matrix must have been formerly initialized with <dismat_init>.
 */
void distmat_compute_from_graph(DistMatrix* mat, TSPGraph const* graph);

/**
 * \brief Gets the distance between two towns
 * \param[in] mat The distance matrix
 * \param[in] i The first town
 * \param[in] j The second town
 * \return The distance between the two towns
 */
float distmat_get(DistMatrix const* mat, int i, int j);

/**
 * \brief Reduces a distance matrix
 * \param[in] mat The distance matrix
 * \return The total value that was deducted from the matrix
 *
 * The matrix is reduced by substracting the minimal value in each row and column such that
 * for all rows and columns there exists at least one zero coefficient.
 */
float distmat_reduce(DistMatrix* mat);

/**
 * \brief Finds the zero coefficient having the maximal penalty in a reduced distance matrix
 * \param[in] mat The reduced distance matrix
 * \param[out] max_pen_zero The position in the matrix of the zero having the maximal penalty
 */
void distmat_find_max_penalty_zero(DistMatrix const* mat, MatPos* max_pen_zero);

/**
 * \brief Converts a position in a distance matrix to its corresponding edge
 * \param[in] mat The distance matrix
 * \param[in] pos The position in the matrix
 * \param[out] edge The edge corresponding to the given matrix position
 */
void distmat_pos_to_edge(DistMatrix const* mat, MatPos const* pos, Edge* edge);

/**
 * \brief Creates a copy of a distance matrix, excluding one given row and column
 * \param[in] mat The distance matrix to copy
 * \param[in] pos The position of the coefficient at the intersection of the row and column to exclude
 * \param[out] new_mat A copy of matrix, without the specified row and column
 *
 * The destination matrix (new_mat) must have been formerly initialized and of order (n - 1), where 
 * n is the order of the source matrix (mat).
 */
void distmat_delete_row_col(DistMatrix const* mat, MatPos const* pos, DistMatrix* new_mat);

/**
 * \brief Bans a given edge in a distance matrix
 * \param[in] mat The distance matrix
 * \param[in] edge The edge to ban
 *
 * The edge is banned from the set of valid edges by setting its corresponding coefficient to 
 * infinity.
 */
void distmat_ban_edge(DistMatrix* mat, Edge const* edge);

/**
 * \brief Bans the reverse edge of a given edge in a distance matrix
 * \param[in] mat The distance matrix
 * \param[in] edge The edge for which to ban the reverse edge
 *
 * The edge is banned from the set of valid edges by setting its corresponding coefficient to 
 * infinity
 */
void distmat_ban_reverse_edge(DistMatrix* mat, Edge const* edge);

/**
 * \brief Prints a matrix to the standard output
 * \param[in] mat The distance matrix
 */
void distmat_print(DistMatrix const* mat);

/**
 * \brief Frees all the memory associated with a distance matrix
 * \param[in] mat The distance matrix
 */
void distmat_free(DistMatrix* mat);

#endif // DIST_MATRIX_H_INCLUDED

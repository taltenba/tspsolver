/**
 * \file
 * \brief 	Simple timer, able to precisely measure elapsed time between two dates.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED

#ifdef _WIN32
#include <windows.h>
#else
#include <time.h>
#endif

/**
 * \typedef Timer
 * \brief A timer
 */
#ifdef _WIN32
typedef LARGE_INTEGER Timer;
#else
typedef struct timespec Timer;
#endif

/**
 * \brief Starts a timer
 * \param[in] timer The timer
 *
 * If the timer has already been started, the latter is reset and started again.
 */
void timer_start(Timer* timer);

/**
 * \brief Stops a formerly started timer
 * \param[in] timer The timer
 *
 * Calling this function with an already stopped timer results in undefined behavior.
 */
void timer_stop(Timer* timer);

/**
 * \brief Gets the time that two elasped between the last start and stop of a timer
 * \param[in] The timer
 * \return The time in seconds between the last start and stop of a timer
 *
 * Calling this function with a timer that has not been formerly started then stopped results in
 * undefined behavior.
 */
float timer_get_elapsed_time(Timer const* timer);

#endif // TIMER_H_INCLUDED

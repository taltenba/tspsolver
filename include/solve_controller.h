/**
 * \file
 * \brief 	Solve a travelling salesman problem (TSP) given user input.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#ifndef SOLVE_CONTROLLER_H_INCLUDED
#define SOLVE_CONTROLLER_H_INCLUDED

/**
 * \brief Solves a TSP according to the given command-line arguments
 * \param[in] argc Number of command-line arguments
 * \param[in] argv The command-line arguments
 * \return 0 on success, a negative value on failure (invalid arguments or out-of-memory)
 */
int solvecontroller_execute(int argc, char** argv);

#endif // SOLVE_CONTROLLER_H_INCLUDED

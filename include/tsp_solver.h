/**
 * \file
 * \brief 	Travelling salesman problem (TSP) solver using two different version of the Little's 
 *          algorithm.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef TSP_SOLVER_H_INCLUDED
#define TSP_SOLVER_H_INCLUDED

#include <stdbool.h>
#include <tsp_graph.h>
#include <dist_matrix.h>
#include <tsp_solution_builder.h>
#include <tsp_solution.h>

#define TSP_ALGO_COUNT 2    /**< Number of versions of the Little's algorithm implemented */

/**
 * \enum TSPAlgorithm
 * \brief Enumerates the different versions of the Little's implemented
 */
typedef enum
{
	LITTLE = 0,    /**< "Little", investigates invalid solutions (containing subtours) */
	LITTLE_PLUS    /**< "Little+", investigates only valid solutions */
} TSPAlgorithm;

/**
 * \struct TSPSolver
 * \brief A TSP solver
 */
typedef struct
{
	DistMatrix* dist_matrices; /**< The distances matrices, indexed by iteration number */
	int* buffer;               /**< General-purpose buffer preventing from doing repetitive malloc */
	TSPSolutionBuilder sol_builder; /**< Solution builder */
	TSPSolution sol;           /**< The current best solution */
	TSPAlgorithm algo;         /**< The version of the Little's algorithm used */
	int town_count;            /**< The number of towns in the TSP graph */
	int max_town_count;        /**< The maximum size of TSP graph that this solver can process */
	bool verbose;              /**< Sets to true if the solver must be verbose, false otherwise */ 
} TSPSolver;

/**
 * \brief Initializes a TSP solver
 * \param[out] solver The intialized TSP solver
 * \param[in] max_town_count The maximum size of TSP graph that the solver would have to process
 * \param[in] verbose Sets to true if the solver must be verbose, false otherwise
 * \return true on success, a negative value otherwise (out-of-memory)
 */
int tspsolver_init(TSPSolver* solver, int max_town_count, bool verbose);

/**
 * \brief Solves a TSP
 * \param[in] solver The TSP solver
 * \param[in] graph The TSP graph, its size must be at most the maximum size that was given to
 *                  <tspsolver_init> when the solver was initialized.
 * \param[in] algo The version of the Little's algorithm that must be used to solve the TSP
 * \return The solution of the TSP
 *
 * The solver must not be freed or used to solve another TSP until the returned solution is no more 
 * in use.
 */
TSPSolution const* tspsolver_solve(TSPSolver* solver, TSPGraph const* graph, TSPAlgorithm algo);

/**
 * \brief Frees all the memory associated with a TSP solver
 * \param[in] solver The TSP solver
 */
void tspsolver_free(TSPSolver* solver);

#endif // TSP_SOLVER_H_INCLUDED

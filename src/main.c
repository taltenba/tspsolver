/**
 * \file
 * \brief 	Utility to solve travelling salesman problems and evaluate the performances of the 
 *          solver. 
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <solve_controller.h>
#include <time_controller.h>

/**
 * \brief Prints the help of the program
 * \param[in] argv The command-line arguments
 */
static void print_help(char** argv)
{
	puts("Usage:");
	printf("\t%s solve <graph_file> <town_count> [-l | -lp]\n", argv[0]);
	printf("\t%s time <graph_file> <start> <end> <step> [-l] [-lp]\n", argv[0]);
}

int main(int argc, char** argv)
{
	if (argc < 3)
	{
		puts("Error: invalid number of arguments.");
		print_help(argv);
		return EXIT_FAILURE;
	}
	
	bool success = false;
	
	if (strcmp(argv[1], "solve") == 0)
		success = solvecontroller_execute(argc, argv) == 0;
	else if (strcmp(argv[1], "time") == 0)
		success = timecontroller_execute(argc, argv) == 0;
	else
		puts("Error: invalid arguments.");
		
	if (!success)
	{
		print_help(argv);
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

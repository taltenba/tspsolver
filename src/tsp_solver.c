/**
 * \file
 * \brief 	Travelling salesman problem (TSP) solver using Little's algorithm
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <tsp_solver.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <assert.h>

int tspsolver_init(TSPSolver* solver, int max_town_count, bool verbose)
{
	assert(solver != NULL);
	assert(max_town_count > 0);
	
	int i = 0;
	
	solver->dist_matrices = NULL;
	solver->buffer = NULL;
	
	solver->dist_matrices = malloc((max_town_count - 1) * sizeof(DistMatrix));
	solver->buffer = malloc(max_town_count * sizeof(int));
	
	if (!solver->dist_matrices || !solver->buffer)
		goto fail;
	
	for (; i < max_town_count - 1; ++i)
	{
		if (distmat_init(&solver->dist_matrices[i], max_town_count - i) != 0)
			goto fail;
	}
	
	if (tspsol_init(&solver->sol, max_town_count) != 0)
		goto fail;
	
	if (tspsolbuilder_init(&solver->sol_builder, max_town_count) != 0)
		goto fail;
	
	solver->max_town_count = max_town_count;
	solver->verbose = verbose;
	return 0;
	
fail:
	free(solver->dist_matrices);
	free(solver->buffer);
	
	while (--i >= 0)
		distmat_free(&solver->dist_matrices[i]);
	
	tspsol_free(&solver->sol);
	return -1;
}

/**
 * \brief Computes the initial solution of a TSP using the neareast neighbour algorithm
 * \param[in] solver The TSP solver
 */
static void compute_init_solution(TSPSolver* solver)
{
	int town_count = solver->town_count;
	int remaining_count = town_count - 1;
	int* remaining_towns = solver->buffer;
	
	for (int i = 1; i < town_count; ++i)
		remaining_towns[i - 1] = i;
	
	DistMatrix* dist_mat = &solver->dist_matrices[0];
	int* sol_route = solver->sol.route;
	float sol_eval = 0.0f;
	
    sol_route[0] = 0;

	for (int i = 1; i < town_count; ++i)
	{
		int cur_town = sol_route[i - 1];
		float min_dist = distmat_get(dist_mat, cur_town, remaining_towns[0]);
		int closest_town = 0;
		
		for (int j = 1; j < remaining_count; ++j)
		{
			float cur_dist = distmat_get(dist_mat, cur_town, remaining_towns[j]);
			
			if (cur_dist < min_dist)
			{
				min_dist = cur_dist;
				closest_town = j;
			}
		}

		sol_route[i] = remaining_towns[closest_town];
		sol_eval += min_dist;
		
		remaining_towns[closest_town] = remaining_towns[remaining_count - 1];
		--remaining_count;
	}

	solver->sol.eval = sol_eval + distmat_get(dist_mat, sol_route[town_count - 1], 0);
}

/**
 * \brief Updates the best solution currently found of a TSP
 * \param[in] solver The TSP solver
 * \param[in] sol_eval The evaluation of the solution
 */
static void update_solution(TSPSolver* solver, float sol_eval)
{
	assert(sol_eval < solver->sol.eval);
	
	bool valid_sol = tspsolbuilder_build(&solver->sol_builder, sol_eval, &solver->sol) == 0;
	
	if (valid_sol && solver->verbose)
	{
		printf("New best solution ");
		tspsol_print(&solver->sol);
	}
}

/**
 * \brief Finds the edge that, added to the current route, would create the longest possible subtour 
 *        including the last chosen edge.
 * \param[in] solver The TSP solver
 * \param[in] iter The current iteration
 * \param[out] subtour_edge The edge that, added to the current route, would create the longest possible subtour 
 *        i                 ncluding the last chosen edge.
 */
static void find_longest_subtour_edge(TSPSolver const* solver, int iter, Edge* subtour_edge)
{
	TSPSolutionBuilder const* sol_builder = &solver->sol_builder;
	
	Edge const* last_chosen_edge = tspsolbuilder_peek_edge(sol_builder);
	
	// Source and end of the longest found path in the current route,
	// including the last chosen edge.
	int longest_path_src = last_chosen_edge->src;
	int longest_path_dest = last_chosen_edge->dest;
	 
	bool found_new_edge;
	
	int prev_src = tspsolbuilder_get_src_of(sol_builder, longest_path_src);
	int next_dest = tspsolbuilder_get_dest_of(sol_builder, longest_path_dest);
	
	do
	{
		found_new_edge = false;
		
		if (prev_src != -1)
		{
			longest_path_src = prev_src;
			found_new_edge = true;
			prev_src = tspsolbuilder_get_src_of(sol_builder, longest_path_src);
		}
		
		if (next_dest != -1)
		{
			longest_path_dest = next_dest;
			found_new_edge = true;
			next_dest = tspsolbuilder_get_dest_of(sol_builder, longest_path_dest);
		}
	} while (found_new_edge);
	
	subtour_edge->src = longest_path_dest;
	subtour_edge->dest = longest_path_src;
}

/**
 * \brief Finds the optimal solution of a TSP using the Little's algorithm
 * \param[in] solver The TSP solver
 * \param[in] iter The current iteration (initially 0)
 * \param[in] eval_parent_node The current minimal evaluation of a solution from the parent node
 *                             (initially 0.0f).
 *
 * Note that the graph of the TSP to solve must at least contains two towns.
 */
static void find_optimal_solution(TSPSolver* solver, int iter, float eval_parent_node)
{	
	DistMatrix* dist_mat = &solver->dist_matrices[iter];
	
	// Reduce the distance matrix and update evaluation of the current node
	float eval_cur_node = eval_parent_node + distmat_reduce(dist_mat);
	
	// Faster to test before if eval_cur_node is infinity
	if (isinf(eval_cur_node) || eval_cur_node >= solver->sol.eval)
		return; // Cut, useless to continue exploring from this node.
	
	// Find the zero with the maximum penalty
	MatPos max_pen_zero;
	distmat_find_max_penalty_zero(dist_mat, &max_pen_zero);
	
	// Add the edge corresponding to that zero to the current route
	Edge chosen_edge;
	distmat_pos_to_edge(dist_mat, &max_pen_zero, &chosen_edge);
	tspsolbuilder_push_edge(&solver->sol_builder, &chosen_edge);
	
	// Check if this is the second to last iteration
	if (iter == solver->town_count - 2)
	{
		// In that case, we have a 2*2 distance matrix, the last edge to take
		// is therefore situated at the position in the matrix that is symetric
		// to the position of the edge we have taken at the current iteration.
		MatPos final_pos;
		final_pos.x = max_pen_zero.x ^ 1;
		final_pos.y = max_pen_zero.y ^ 1;
		
		assert(distmat_get(dist_mat, final_pos.x, final_pos.y) < FLT_EPSILON);
		
		distmat_pos_to_edge(dist_mat, &final_pos, &chosen_edge);
		tspsolbuilder_push_edge(&solver->sol_builder, &chosen_edge);
		
		// Update the solution with the new best solution found
		update_solution(solver, eval_cur_node);
		
		// Removes to two last chosen edges from the current route
		tspsolbuilder_pop_edge(&solver->sol_builder);
		tspsolbuilder_pop_edge(&solver->sol_builder);
		
		return;
	}
	
	// Copy the current matrix to the new one, without the row and column of the
	// edge we have chosen and banning the reverse edge to avoid two-town subtours.
	DistMatrix* new_mat = &solver->dist_matrices[iter + 1];
	distmat_delete_row_col(dist_mat, &max_pen_zero, new_mat);
	
	if (solver->algo == LITTLE)
	{
		distmat_ban_reverse_edge(new_mat, &chosen_edge);
	}
	else
	{
		Edge longest_subtour_edge;
		find_longest_subtour_edge(solver, iter, &longest_subtour_edge);
		distmat_ban_edge(new_mat, &longest_subtour_edge);
	}
	
	// Explore the left part of the tree (in which we take the edge we have chosen) 
	find_optimal_solution(solver, iter + 1, eval_cur_node);
	
	// Explore the right part of the tree (in which we do not take the edge we have chosen)
	tspsolbuilder_pop_edge(&solver->sol_builder);
	distmat_ban_edge(dist_mat, &chosen_edge);
	find_optimal_solution(solver, iter, eval_cur_node);
}

TSPSolution const* tspsolver_solve(TSPSolver* solver, TSPGraph const* graph, TSPAlgorithm algo)
{
	assert(solver != NULL);
	assert(graph != NULL);
	assert(solver->max_town_count >= tspgraph_get_size(graph));
	
	solver->town_count = tspgraph_get_size(graph);
	solver->algo = algo;
	
	if (solver->town_count == 1)
	{
		solver->sol.route[0] = 0;
		solver->sol.eval = 0.0f;
		solver->sol.town_count = 1;
	}
	else
	{
		// Use the matrix of size town_count as first matrix
		solver->dist_matrices += solver->max_town_count - solver->town_count;
		
		distmat_compute_from_graph(&solver->dist_matrices[0], graph);
		compute_init_solution(solver);
		
		if (solver->verbose)
		{
			puts("Points coordinates:");
			tspgraph_print(graph);
			putchar('\n');
			
			puts("Distance matrix:");
			distmat_print(&solver->dist_matrices[0]);
			putchar('\n');
			
			printf("Initial solution ");
			tspsol_print(&solver->sol);
		}
		
		find_optimal_solution(solver, 0, 0.0f);
		
		// Reset the matrix pointer to the matrix of size max_town_count
		solver->dist_matrices -= solver->max_town_count - solver->town_count;
	}
	
	if (solver->verbose)
	{
		printf("\nBest solution ");
		tspsol_print(&solver->sol);
	}
	
	return &solver->sol;
}

void tspsolver_free(TSPSolver* solver)
{
	assert(solver != NULL);
	
	int max_town_count = solver->max_town_count;
	
	for (int i = 0; i < max_town_count - 1; ++i)
		distmat_free(&solver->dist_matrices[i]);
	
	tspsol_free(&solver->sol);
	tspsolbuilder_free(&solver->sol_builder);
	
	free(solver->dist_matrices);
	free(solver->buffer);
}

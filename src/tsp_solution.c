/**
 * \file
 * \brief 	Represents a solution of the travelling salesman problem (TSP)
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <tsp_solution.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int tspsol_init(TSPSolution* sol, int town_count)
{
	assert(sol != NULL);
	assert(town_count > 0);
	
	sol->route = malloc(town_count * sizeof(int));
	
	if (!sol->route)
		return -1;
		
	sol->town_count = town_count;
	return 0;
}

void tspsol_print(TSPSolution const* sol)
{
	assert(sol != NULL);
	
	printf("(%.2f): ", sol->eval);
	
    for (int i = 0, end = sol->town_count; i < end; ++i)
        printf("%d ", sol->route[i]);

    printf("\n");
}

void tspsol_free(TSPSolution* sol)
{
	assert(sol != NULL);
	free(sol->route);
}

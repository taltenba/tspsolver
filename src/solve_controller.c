/**
 * \file
 * \brief 	Solve a travelling salesman problem (TSP) given user input.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <solve_controller.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <tsp_solver.h>
#include <string_util.h>
#include <timer.h>
#include <tsp_graph.h>

/**
 * \struct SolveArgs
 * \brief The parsed arguments from the command-line arguments
 */
typedef struct
{
	char* graph_file;
	int town_count;
	TSPAlgorithm algo;
} SolveArgs;

/**
 * \brief Parses the command-line arguments
 * \param[in] argc The number of command-line arguments
 * \param[in] argv The command-line arguments
 * \param[out] args The parsed arguments
 * \return 0 on success, -1 otherwise
 */
static int parse_solve_args(int argc, char** argv, SolveArgs* args)
{
	if (argc < 4 || argc > 5)
	{
		puts("Error: invalid number of arguments.");
		return -1;
	}
	
	args->graph_file = argv[2];
	
	if (parse_int(argv[3], &args->town_count) != 0 || args->town_count < 1)
	{
		puts("Error: invalid number of towns, must be stricly positive integer.");
		return -1;
	}
	
	if (argc == 4 || strcmp(argv[4], "-lp") == 0)
	{
		args->algo = LITTLE_PLUS;
		return 0;
	}
	
	if (strcmp(argv[4], "-l") == 0)
	{
		args->algo = LITTLE;
		return 0;
	}
	
	puts("Error: invalid option.");
	return -1;	
}

int solvecontroller_execute(int argc, char** argv)
{
	assert(argc > 1);
	assert(argv != NULL);
	
	SolveArgs args;
	
	if (parse_solve_args(argc, argv, &args) != 0)
		return -1;
	
	TSPGraph graph;
	
	if (tspgraph_load(&graph, args.graph_file, args.town_count) != 0)
	{
		puts("Error: invalid graph file.");
		return -1;
	}
	
	TSPSolver solver;
	
	if (tspsolver_init(&solver, args.town_count, true) != 0)
	{
		puts("Error: out of memory.");
		tspgraph_free(&graph);
		return -1;
	}
	
	Timer timer;
	
	timer_start(&timer);
	tspsolver_solve(&solver, &graph, args.algo);
	timer_stop(&timer);
	
	printf("Elaspsed time: %.6f s\n", timer_get_elapsed_time(&timer));
	
	tspsolver_free(&solver);
	tspgraph_free(&graph);
	return 0;
}

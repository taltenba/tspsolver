/**
 * \file
 * \brief 	Travelling salesman problem (TSP) solution builder
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <tsp_solution_builder.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int tspsolbuilder_init(TSPSolutionBuilder* builder, int town_count)
{
	assert(builder != NULL);
	
	builder->chosen_edges = NULL;
	builder->src_index_map = NULL;
	builder->dest_index_map = NULL;
	builder->buffer = NULL;
	
	builder->chosen_edges = malloc(town_count * sizeof(Edge));
	builder->src_index_map = malloc(town_count * sizeof(int));
	builder->dest_index_map = malloc(town_count * sizeof(int));
	builder->buffer = malloc(3 * town_count * sizeof(int));
	
	if (!builder->chosen_edges || !builder->src_index_map || !builder->dest_index_map ||
	    !builder->buffer)
    {
    	goto fail;
    }
		
	memset(builder->src_index_map, -1, town_count * sizeof(int));
	memset(builder->dest_index_map, -1, town_count * sizeof(int));
	
	builder->count = 0;
	
	return 0;
	
fail:
	free(builder->chosen_edges);
	free(builder->src_index_map);
	free(builder->dest_index_map);
	
	return -1;
}

void tspsolbuilder_push_edge(TSPSolutionBuilder* builder, Edge const* edge)
{
	assert(builder != NULL);
	assert(edge != NULL);
	
	int count = builder->count;
	
	builder->chosen_edges[count] = *edge;
	builder->src_index_map[edge->src] = count;
	builder->dest_index_map[edge->dest] = count;
	
	++builder->count;
}

void tspsolbuilder_pop_edge(TSPSolutionBuilder* builder)
{
	assert(builder != NULL);
	assert(builder->count > 0);
	
	Edge* removed = &builder->chosen_edges[--builder->count];
	builder->src_index_map[removed->src] = -1;
	builder->dest_index_map[removed->dest] = -1;
}

Edge const* tspsolbuilder_peek_edge(TSPSolutionBuilder const* builder)
{
	assert(builder != NULL);
	return &builder->chosen_edges[builder->count - 1];
}

int tspsolbuilder_get_src_of(TSPSolutionBuilder const* builder, int dest_town)
{
	assert(builder != NULL);
	
	int ind = builder->dest_index_map[dest_town];
	
	if (ind == -1)
		return -1;
		
	return builder->chosen_edges[ind].src;
}

int tspsolbuilder_get_dest_of(TSPSolutionBuilder const* builder, int src_town)
{
	assert(builder != NULL);
	
	int ind = builder->src_index_map[src_town];
	
	if (ind == -1)
		return -1;
	
	return builder->chosen_edges[ind].dest;
}

int tspsolbuilder_build(TSPSolutionBuilder const* builder, float sol_eval, TSPSolution* sol)
{
	assert(builder != NULL);
	assert(sol_eval >= 0.0f);
	assert(sol != NULL);
	
	int town_count = builder->count;
	Edge* chosen_edges = builder->chosen_edges;
	
	// Create a mapping between source and destination towns
	int* src_dest_map = builder->buffer;
	
	for (int i = 0; i < town_count; ++i)
		src_dest_map[chosen_edges[i].src] = chosen_edges[i].dest;
	
	int* sol_route = &builder->buffer[town_count];
	int* visited = &builder->buffer[2 * town_count];
	
	memset(visited, 0, town_count * sizeof(int));
	
	// Start the route with the town n°0
	sol_route[0] = 0;
	visited[0] = 1;
	
	for (int i = 1; i < town_count; ++i)
	{
		int cur_town = sol_route[i - 1];
		int next_town = src_dest_map[cur_town];
		
		if (visited[next_town])
			return -1; // Invalid solution, contains a subtour
		
		sol_route[i] = next_town;
		visited[next_town] = 1;
	}
	
	memcpy(sol->route, sol_route, town_count * sizeof(int));
	sol->eval = sol_eval;
	sol->town_count = town_count;
	
	return 0;
}

void tspsolbuilder_free(TSPSolutionBuilder* builder)
{
	assert(builder != NULL);
	
	free(builder->chosen_edges);
	free(builder->src_index_map);
	free(builder->dest_index_map);
	free(builder->buffer);
}

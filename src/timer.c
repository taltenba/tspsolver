/**
 * \file
 * \brief 	Simple timer, able to measure elapsed time between two dates.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <timer.h>

void timer_start(Timer* timer)
{
#ifdef _WIN32
	QueryPerformanceCounter(timer);
#else
	clock_gettime(CLOCK_MONOTONIC, timer);
#endif
}

void timer_stop(Timer* timer)
{
#ifdef _WIN32
	LARGE_INTEGER end;
	QueryPerformanceCounter(&end);
	timer->QuadPart = end.QuadPart - timer->QuadPart;
#else
	struct timespec end;
	clock_gettime(CLOCK_MONOTONIC, &end);
	timer->tv_sec = end.tv_sec - timer->tv_sec;
	timer->tv_nsec = end.tv_nsec - timer->tv_nsec;
#endif
}

float timer_get_elapsed_time(Timer const* timer)
{
#ifdef _WIN32
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	return (float) timer->QuadPart / freq.QuadPart;
#else
	return timer->tv_sec + timer->tv_nsec / 1e9f;
#endif
}

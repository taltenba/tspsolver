/**
 * \file
 * \brief 	Evaluates the travelling salesman problem (TSP) solver performances given user input.
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <time_controller.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <tsp_solver.h>
#include <string_util.h>
#include <timer.h>
#include <tsp_graph.h>

/**
 * \struct TimeArgs
 * \brief The parsed arguments from the command-line arguments
 */
typedef struct
{
	char* graph_file;
	int start;
	int end;
	int step;
	bool algos[TSP_ALGO_COUNT];
} TimeArgs;

/**
 * \brief Parses the command-line arguments
 * \param[in] argc The number of command-line arguments
 * \param[in] argv The command-line arguments
 * \param[out] args The parsed arguments
 * \return 0 on success, -1 otherwise
 */
static int parse_time_args(int argc, char** argv, TimeArgs* args)
{
	if (argc < 6 || argc > 8)
	{
		puts("Error: invalid number of arguments.");
		return -1;
	}
	
	args->graph_file = argv[2];
	
	bool success = parse_int(argv[3], &args->start) == 0 && args->start > 0
				&& parse_int(argv[4], &args->end) == 0 && args->end > 0
				&& parse_int(argv[5], &args->step) == 0 && args->step > 0;
				
	if (!success)
	{
		puts("Error: invalid argument values, <start> <end> and <step> must be "
		     "strictly positive integers.");
		return -1;
	}
	
	if (args->end < args->start)
	{
		puts("Error: invalid argument values, <start> must be greater than or equal to <end>.");
		return -1;
	}
	
	if (argc == 6)
	{
		memset(&args->algos, true, TSP_ALGO_COUNT);
		return 0;
	}
	
	memset(&args->algos, false, TSP_ALGO_COUNT);
	
	int i;
	
	for (i = 6; i < argc; ++i)
	{
		if (strcmp(argv[i], "-l") == 0)
			args->algos[LITTLE] = true;
		else if (strcmp(argv[i], "-lp") == 0)
			args->algos[LITTLE_PLUS] = true;
		else
			break;
	}
	
	if (i < argc)
	{
		puts("Error: invalid option.");
		return -1;
	}
	
	return 0;
}

int timecontroller_execute(int argc, char** argv)
{
	assert(argc > 1);
	assert(argv != NULL);
	
	TimeArgs args;
	
	if (parse_time_args(argc, argv, &args) != 0)
		return -1;
	
	TSPGraph graph;
	
	if (tspgraph_load(&graph, args.graph_file, args.end) != 0)
	{
		puts("Error: invalid graph file.");
		return -1;
	}
	
	TSPSolver solver;
	
	if (tspsolver_init(&solver, args.end, false) != 0)
	{
		puts("Error: out of memory.");
		tspgraph_free(&graph);
		return -1;
	}
	
	printf("towns");
	
	for (int i = 0; i < TSP_ALGO_COUNT; ++i)
	{
		if (!args.algos[i])
			continue;
			
		if (i == LITTLE)
			printf("\t  Little (s)");
		else
			printf("\t Little+ (s)");
	}
	
	putchar('\n');
	
	Timer timer;
	
	for (int i = args.start; i <= args.end; i += args.step)
	{
		printf("%5d", i);
	
		for (int j = 0; j < TSP_ALGO_COUNT; ++j)
		{
			if (!args.algos[j])
				continue;
				
			TSPGraph subgraph;
			tspgraph_get_subgraph(&graph, i, &subgraph);
			
			timer_start(&timer);
			tspsolver_solve(&solver, &subgraph, j);
			timer_stop(&timer);
			
			printf("\t%12.6f", timer_get_elapsed_time(&timer));
		}
		
		putchar('\n');
	}
	
	tspsolver_free(&solver);
	tspgraph_free(&graph);
	return 0;
}

/**
 * \file
 * \brief 	Graph of a travelling salesman problem (TSP)
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <tsp_graph.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <string_util.h>

#define IO_BUFFER_SIZE 1024 /**< Size in bytes of the buffer use for IO */

int tspgraph_load(TSPGraph* graph, char* file_path, int size)
{
	FILE* file = fopen(file_path, "r");
	
	if (!file)
		return -1;
	
	graph->towns = malloc(size * sizeof(Node));
	
	if (!graph->towns)
		goto fail;
	
	char buffer[IO_BUFFER_SIZE];
	
	for (int i = 0; i < size; ++i)
	{
		if (fgets(buffer, IO_BUFFER_SIZE, file) == NULL)
			goto fail;
		
		bool success = parse_float(strtok(buffer, " "), &graph->towns[i].x) == 0
					&& parse_float(strtok(NULL, " "), &graph->towns[i].y) == 0;
		
		if (!success)
			goto fail;
	}
	
	fclose(file);
	graph->size = size;
	return 0;
	
fail:
	free(graph->towns);
	fclose(file);
	return -1;
}

float tspgraph_compute_dist(TSPGraph const* graph, int i, int j)
{
	assert(graph != NULL);
	assert(i < graph->size && j < graph->size);
	
	Node const* towns = graph->towns;
	
	float delta_x = towns[j].x - towns[i].x;
	float delta_y = towns[j].y - towns[i].y;

	return sqrt(delta_x * delta_x + delta_y * delta_y);
}

int tspgraph_get_size(TSPGraph const* graph)
{
	assert(graph != NULL);
	return graph->size;
}

void tspgraph_get_subgraph(TSPGraph const* graph, int subsize, TSPGraph* subgraph)
{
	assert(graph != NULL);
	assert(subgraph != NULL);
	assert(subsize <= graph->size);
	
	subgraph->towns = graph->towns;
	subgraph->size = subsize;
}

void tspgraph_print(TSPGraph const* graph)
{
	assert(graph != NULL);
	
	int size = graph->size;
	Node const* towns = graph->towns;
	
	for (int i = 0; i < size; ++i)
		printf("Node %d: x=%8.2f, y=%8.2f\n", i, towns[i].x, towns[i].y);
}

void tspgraph_free(TSPGraph* graph)
{
	assert(graph != NULL);
	free(graph->towns);
}

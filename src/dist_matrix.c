/**
 * \file
 * \brief 	Distance matrix of a travelling salesman problem (TSP)
 * \date    Created:  17/04/2019
 * \date    Modified: 24/04/2019
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */
 
#include <dist_matrix.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <assert.h>

int distmat_init(DistMatrix* mat, int size)
{
	assert(mat != NULL);
	assert(size > 0);
	
	mat->col_towns = NULL;
	mat->row_towns = NULL;
	mat->dists = NULL;
	
	mat->col_towns = malloc(size * sizeof(int));
	mat->row_towns = malloc(size * sizeof(int));
	mat->dists = malloc(size * size * sizeof(float));
	
	if (!mat->col_towns || !mat->row_towns || !mat->dists)
		goto fail;
	
	mat->size = size;
	
	return 0;
	
fail:
	free(mat->col_towns);
	free(mat->row_towns);
	free(mat->dists);
	
	return -1;
}

void distmat_compute_from_graph(DistMatrix* mat, TSPGraph const* graph)
{
	assert(mat != NULL);
	assert(graph != NULL);
	assert(tspgraph_get_size(graph) == mat->size);
	
	int size = mat->size;
	float (*dists)[size] = (float(*)[size]) mat->dists;
	
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < i; ++j)
		{
			// Distance matrix is symetric
			float d = tspgraph_compute_dist(graph, i, j);
			dists[i][j] = d;
			dists[j][i] = d;
		}
		
		// Prevents subtours of one town
		dists[i][i] = INFINITY;
		
		mat->row_towns[i] = i;
		mat->col_towns[i] = i;
	}
}

float distmat_get(DistMatrix const* mat, int i, int j)
{
	return mat->dists[i * mat->size + j];
}

/**
 * \brief Finds the minimum of a row of a distance matrix
 * \param[in] size The order of the matrix
 * \param[in] dists The coefficients of the matrix
 * \param[in] row The index of the row
 * \return The minimal distance in the row
 */
static float find_min_row_dist(int size, float (*dists)[size], int row)
{
	float row_min = INFINITY;
	
	for (int i = 0; i < size; ++i)
	{
		if (row_min > dists[row][i])
			row_min = dists[row][i];
	}
	
	return row_min;
}

/**
 * \brief Finds the minimum of a column of a distance matrix
 * \param[in] size The order of the matrix
 * \param[in] dists The coefficients of the matrix
 * \param[in] col The index of the column
 * \return The minimal distance in the column
 */
static float find_min_col_dist(int size, float (*dists)[size], int col)
{
	float col_min = INFINITY;
	
	for (int i = 0; i < size; ++i)
	{
		if (col_min > dists[i][col])
			col_min = dists[i][col];
	}
	
	return col_min;
}

/**
 * \brief Subtracts a value from all the coefficient of a distance matrix row
 * \param[in] size The order of the matrix
 * \param[in] dists The coefficients of the matrix
 * \param[in] row The index of the row
 * \param[in] value The value to subtract
 */
static void sub_dist_row(int size, float (*dists)[size], int row, float value)
{
	for (int i = 0; i < size; ++i)
		dists[row][i] -= value;
}

/*
 * \brief Subtracts a value from all the coefficient of a distance matrix column
 * \param[in] size The order of the matrix
 * \param[in] dists The coefficients of the matrix
 * \param[in] col The index of the column
 * \param[in] value The value to subtract
 */
static void sub_dist_col(int size, float (*dists)[size], int col, float value)
{
	for (int i = 0; i < size; ++i)
		dists[i][col] -= value;
}

float distmat_reduce(DistMatrix* mat)
{
	// Note: it has been tried to store the zero coefficient positions during the
	// traversal of the matrix to avoid traversing again the whole matrix in order
	// to compute the penalties but tests have shown that it has negative effect on
	// performances. It is better in this case to write two "small" double-for
	// loops, each having a unique goal, than writing one bigger double-for loop
	// for the two goals.
	
	assert(mat != NULL);
	
	int size = mat->size;
	float (*dists)[size] = (float(*)[size]) mat->dists;
	float min_sum = 0.0f;
	
	for (int i = 0; i < size; ++i)
	{
		float min_row_dist = find_min_row_dist(size, dists, i);
		
		if (min_row_dist > FLT_EPSILON)
		{
			if (isinf(min_row_dist))
				return INFINITY;
		
			min_sum += min_row_dist;
			sub_dist_row(size, dists, i, min_row_dist);
		}
	}
	
	for (int i = 0; i < size; ++i)
	{
		float min_col_dist = find_min_col_dist(size, dists, i);
		
		if (min_col_dist > FLT_EPSILON)
		{
			if (isinf(min_col_dist))
				return INFINITY;
		
			min_sum += min_col_dist;
			sub_dist_col(size, dists, i, min_col_dist);
		}
	}
	
	return min_sum;
}

void distmat_find_max_penalty_zero(DistMatrix const* mat, MatPos* max_pen_zero)
{
	assert(mat != NULL);
	assert(max_pen_zero != NULL);
	
	int size = mat->size;
	float (*dists)[size] = (float(*)[size]) mat->dists;
	
	float max_pen = -1.0f;
	
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			if (dists[i][j] > FLT_EPSILON) 
				continue;
			
			float tmp;
			tmp = dists[i][j];
			dists[i][j] = INFINITY; // Ignore this coefficient when searching for the minimums
			
			float pen = find_min_row_dist(size, dists, i) + find_min_col_dist(size, dists, j);
			
			dists[i][j] = tmp;
			
			if (pen > max_pen)
			{
				max_pen = pen;
				max_pen_zero->x = i;
				max_pen_zero->y = j;
			}
		}
	}
}

void distmat_pos_to_edge(DistMatrix const* mat, MatPos const* pos, Edge* edge)
{
	assert(mat != NULL);
	assert(pos != NULL);
	assert(edge != NULL);
	
	edge->src = mat->row_towns[pos->x];
	edge->dest = mat->col_towns[pos->y];
}

/**
 * \brief Copies an array without a specified array position
 * \param[in] src The source array
 * \param[in] pos The position to exclude
 * \param[in] src_length The number of elements in the source array
 * \param[in] e_size The size of one array element, in bytes
 * \param[out] dest The destination array
 */
static void copy_excluding(void const* src, int pos, int src_length, size_t e_size, void* dest)
{
	memcpy(dest, src, pos * e_size);
	memcpy((void*) ((size_t) dest + pos * e_size), (void*) ((size_t) src + (pos + 1) * e_size), (src_length - pos - 1) * e_size);
}

void distmat_delete_row_col(DistMatrix const* mat, MatPos const* pos, DistMatrix* new_mat)
{
	assert(mat != NULL);
	assert(pos != NULL);
	assert(new_mat != NULL);
	assert(new_mat->size == mat->size - 1);
	
	int size = mat->size;
	int deleted_row = pos->x;
	int deleted_col = pos->y;
	
	copy_excluding(mat->row_towns, deleted_row, size, sizeof(int), new_mat->row_towns);
	copy_excluding(mat->col_towns, deleted_col, size, sizeof(int), new_mat->col_towns);
	
	float (*dists)[size] = (float(*)[size]) mat->dists;
	float (*new_dists)[size - 1] = (float(*)[size - 1]) new_mat->dists;
	
	int i;
	
	for (i = 0; i < deleted_row; ++i)
		copy_excluding(dists[i], deleted_col, size, sizeof(float), new_dists[i]);
	
	for (++i; i < size; ++i)
		copy_excluding(dists[i], deleted_col, size, sizeof(float), new_dists[i - 1]);
}

/**
 * \brief Finds a town in a town array
 * \param[in] towns The town array
 * \param[in] size The size of the array
 * \param[in] to_find The town to find
 * \return The index in the array of the town to find if the latter was found, -1 otherwise
 */ 
static int search_town(int* towns, int size, int to_find)
{
	// Town array are sorted so we could use a binary search
	// but our solver is only designed to solve TSP of at 
	// most some decades of towns and in that case a linear 
	// search is usually faster on average.
	
	for (int i = 0; i < size; ++i)
	{
		if (towns[i] == to_find)
			return i;
	}
	
	return -1;
}

void distmat_ban_edge(DistMatrix* mat, Edge const* edge)
{
	assert(mat != NULL);
	assert(edge != NULL);
	
	int size = mat->size;
	
	int i = search_town(mat->row_towns, size, edge->src);
	
	if (i < 0)
		return;
	
	int j = search_town(mat->col_towns, size, edge->dest);
	
	if (j < 0)
		return;
	
	mat->dists[i * size + j] = INFINITY;
}

void distmat_ban_reverse_edge(DistMatrix* mat, Edge const* edge)
{
	Edge reverse = {edge->dest, edge->src};
	distmat_ban_edge(mat, &reverse);
}

void distmat_print(DistMatrix const* mat)
{
	assert(mat != NULL);
	
	int size = mat->size;
	float (*dists)[size] = (float(*)[size]) mat->dists;
	
	printf("   ");
	
	for (int i = 0; i < size; ++i)
		printf("%8d ", mat->col_towns[i]);
	
	putchar('\n');
	
	for (int i = 0; i < size; ++i)
    {
        printf("%2d:", mat->row_towns[i]);

        for (int j = 0; j < size; ++j)
		{
			if (isinf(dists[i][j]))
				printf("    +inf ");
			else
				printf("%8.2f ", dists[i][j]);
		}
        
        putchar('\n');
    }
}

void distmat_free(DistMatrix* mat)
{
	assert(mat != NULL);
	
	free(mat->col_towns);
	free(mat->row_towns);
	free(mat->dists);
}

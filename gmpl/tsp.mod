/**
 * Model for solving travelling salesman problems using the GLPK solver
 *
 * Data files for different size of TSP can be found in data directory.
 * For ten towns, use for example:
 *     glpsol -m tsp.mod -d data/berlin52_10.tsp
 * 
 * Created:  17/04/2018
 * Modified: 24/04/2018
 * Author:   ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

/* Number of towns */
param n;

/* Towns */
set TOWNS := 1..n;

/* Distance matrix */
param d{i in TOWNS, j in TOWNS};

/* x[i,j] = 1 if and only if the salesman goes from town i to town j */
var x{i in TOWNS, j in TOWNS}, binary;

/* Integers used to ensure that the tour is a cycle */
var u{i in TOWNS}, integer, >= 1, <= n;

/* Minimize the total traveled distance */
minimize z: sum{i in TOWNS, j in TOWNS} d[i,j]*x[i,j];

/* The salesman leaves each town exactly once */
s.t. R1{i in TOWNS}: sum{j in TOWNS} x[i,j] = 1;

/* The salesman enters each town exactly once */
s.t. R2{j in TOWNS}: sum{i in TOWNS} x[i,j] = 1;

/* Loops are not allowed */
s.t. R3{i in TOWNS}: x[i,i] = 0;

/* The tour must be a cycle */
s.t. R4: u[1] = 1;
s.t. R5{i in TOWNS: i > 1}: u[i] >= 2;
s.t. R6{i in TOWNS, j in TOWNS: j > 1 and i != j}: u[i] - u[j] + n*x[i,j] <= n - 1;

solve;

printf "Best solution (%f):\n", z;
printf "From\tTo\n";
printf{i in TOWNS, j in TOWNS: x[i,j] = 1} "%4d\t%d\n", i, j;

end;

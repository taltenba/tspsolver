## Set to 1 for debug, 0 for release
DEBUG ?= 0

## Selected compiler
CXX = gcc

## Compilation flags
ifeq ($(DEBUG), 1)
	export CFLAGS ?= -Wall -Werror --std=gnu99 -pedantic -g -O0
else
	export CFLAGS ?= -Wall -Werror --std=gnu99 -pedantic -O2 -DNDEBUG
endif

## Name of the directory where source .c files are stored
SRCDIR = src
## Name of the directory where object .o files are stored
OBJDIR = obj
## Name of the directory where header .h files are stored
INCLUDEDIR = include
## Application name
APP = tspsolver
## Executable file
TARGET = $(APP)
## Source .c files
CFILES = $(shell find $(SRCDIR) -name '*.c')
## Object .o files
OFILES = $(subst $(SRCDIR)/,$(OBJDIR)/, $(patsubst %.c,%.o, $(CFILES)))
## Directories of the libraries to include
LIBSDIRS =
## Names of the libraries to include
LIBS = -lm
## Directories of the header .h files to include
INCLUDEDIRS = -I$(INCLUDEDIR)

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJDIR) $(OFILES)
	$(CXX) $(CFLAGS) $(OFILES) $(LIBSDIRS) $(LIBS) -o $(TARGET)

$(OBJDIR):
	mkdir $@

.PHONY: clean
clean:
	rm -rf $(OBJDIR) $(TARGET)
	
.SECONDEXPANSION:

$(OFILES): %.o: $$(subst $(OBJDIR)/,$(SRCDIR)/, %).c
	$(CXX) $(CFLAGS) $(INCLUDEDIRS) -c -o $@ $<

# TSPSolver
TSPSolver is a C program able to solve travelling salesman problem (TSP) instances using Little's algorithm [[1]](#1).

This project has been carried out as part of the *Optimization and Operational Research* course I attended during my studies. The goal was to implement an efficient version of the Little's algorithm to solve TSP and to compare the performance between that algorithm and the GPLK solver.

The `gmpl` directory contains the model used to solve TSP instances with GPLK solver. That model has been used as reference to measure the GPLK solver performance.   

![Image Imgur](https://i.imgur.com/a6lh27i.png)

## Getting started
### Prerequisites
* Linux or macOS is recommended. The program can also be compiled for Windows but the makefile cannot be used without a console emulator like `cmder`.
* GCC (or MinGW for Windows).

### Building
Simply run the makefile:
```sh
make
```

### TSP graph modeling
The repository already contains a `berlin52.tsp` file which represents a TSP graph built from 52 locations in Berlin. This graph can be used directly to test the program or custom graphs can be modeled by creating a file listing the 2D coordinates of the locations. Each line of the file corresponds to one location and contains its abscissa and ordinate, separated by a space.

Example of the content of a file representing a graph with three locations:
```
100.0 55.7
47.8 21.3
96.2 17.0
```

### Solving a TSP instance
```
.\tsp_solver solve <graph_file> <town_count> [-l | -lp]
```
Where:
- `<graph_file>` is the file modeling the TSP graph.
- `<town_count>` is the TSP graph size (ie. number of locations). If the given size is lower than the actual size of the graph modeled by the specified file, only the first `<town_count>` locations listed in the file will be taken into account.
- `-l` and `-lp` options enable to choose if the solver must respectively use the *Little* or *Little+* implementation of the Little's algorithm. By default the more efficient *Little+* version is used.

### Measuring the resolution time by varying the TSP instance size between two bounds
```
.\tsp_solver time <graph_file> <start> <end> <step> [-l] [-lp]
```
Where:
- `<graph_file>` is the file modeling the TSP graph.
- `<start>` is the size of the first TSP instance to solve (must be lower than or equal to the actual size of the graph modeled by the specified file).
- `<end>` is the maximal TSP instance size (must be lower than or equal to the actual size of the graph modeled by the specified file, and `<end> >= <start>`) 
- `<step>` is the interval between each TSP instance size to solve.
- `-l` and `-lp` options enable to choose if the solver must respectively use the *Little* and/or *Little+* implementation of the Little's algorithm. By default the resolution is performed with both implementations.

## Authors
* Thomas ALTENBACH - @taltenba

## References
<a id="1">[1]</a> 
Little, J. D. C., Murty, K. G., Sweeney, D. W., Karel, C. (1963) *An Algorithm for the Traveling Salesman Problem*. Cambridge, MA: MIT.
